import React from 'react';

function App() {
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [salary, setSalary] = React.useState("");

  const register = async () => {
    await fetch("/register", {
      body: JSON.stringify({ email, password, salary }),
      headers: { Accept: "application/json", "Content-Type": "application/json" },
      method: "POST",
    });
  }

  return (
    <body>
      <input onChange={(event) => setEmail(event.target.value)} value={email} />
      <input onChange={(event) => setPassword(event.target.value)} value={password} />
      <input onChange={(event) => setSalary(event.target.value)} value={salary} />

      <button onClick={register}>Register</button>
    </body>
  );
}

export default App;
