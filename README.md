# Registration Form

The backend is POST to `/register` with a JSON payload of

```
{
  email: string,
  password: string,
  salary: string
}
```

it will respond with 200 on success, and 400 on error. The error
response is

```
{
   code: string,
}
```

where code is `INVALID_EMAIL_DOMAIN`, or `WEAK_PASSWORD`.
